﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetMongoData.Models
{
    public class HourlyCountViewModel
    {
        public DateTime StartTime { get; set; }
        public int TickSpeed { get; set; }
    }
}