﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GetMongoData.Models;

namespace GetMongoData.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HourlyCount(DateTime? startTime, int? tickSpeed)
        {
            var model = new HourlyCountViewModel
            {
                StartTime = startTime.GetValueOrDefault(DateTime.Now.AddHours(-1)),
                TickSpeed = tickSpeed.GetValueOrDefault(1000)
            };

            return View(model);
        }
    }
}
