﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace GetMongoData.Controllers
{
    public class TimeLocationController : ApiController
    {

        public class ClientLocation
        {
            public BsonDateTime timestamp { get; set; }
            public string mac { get; set; }
            public int x { get; set; }
            public int y { get; set; }
        }

        public class Location
        {
            public ObjectId _id { get; set; }
            public BsonDateTime timestamp { get; set; }
            public string mac { get; set; }
            public int x { get; set; }
            public int y { get; set; }
        }

        private List<Location> GetData(DateTime time)
        {
            const string connectionString = "mongodb://52.17.20.213";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("wifitracker");
            var collection = database.GetCollection<Location>("locations");

            var query = Query<Location>.GT(l => l.timestamp, time.AddSeconds(-60));


            var q = from l in collection.AsQueryable<Location>()
                where l.timestamp < time &&
                      l.timestamp > time.AddSeconds(-60)
                select l;

            //var result = collection.Find(query);

            return q.ToList();

        }

        //
        // GET: api/TimeLocation/
        public List<ClientLocation> Get(DateTime fromTime)
        {
            var result = new List<ClientLocation>();

            var locations = GetData(fromTime);

            foreach (var location in locations)
            {
                var client = new ClientLocation
                {
                    mac = location.mac,
                    //timestamp = location.timestamp.ToUniversalTime(),
                    x = location.x,
                    y = location.y
                };
                result.Add(client);
            }

            return result;
        }



    }
}
