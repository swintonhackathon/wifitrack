﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;


namespace GetMongoData.Controllers
{
    public class ValuesController : ApiController
    {
        public class Location
        {
            public ObjectId _id { get; set; }
            public BsonDateTime timestamp { get; set; }
            public string mac { get; set; }
            public int x { get; set; }
            public int y { get; set; }
        }

        private List<Tuple<int, int>> GetData()
        {
            const string connectionString = "mongodb://52.17.20.213";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("wifitracker");
            var collection = database.GetCollection<Location>("locations");

            var query = Query<Location>.GT(l => l.timestamp, DateTime.Now.AddSeconds(-60));
            var result = collection.Find(query);

            return result.Select(location => new Tuple<int, int>(location.x, location.y)).ToList();

        }

        private List<Tuple<int, int>> GsdffetData()
        {
            const string connectionString = "mongodb://52.17.20.213";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("wifitracker");
            var collection = database.GetCollection<Location>("locations");

            var query = Query<Location>.GT(l => l.timestamp, DateTime.Now.AddSeconds(-60));
            var result = collection.Find(query);

            return result.Select(location => new Tuple<int, int>(location.x, location.y)).ToList();

        }
        public List<Location> Get(int fromTime)
        {
            return new List<Location>();
        }

        // GET api/values
        public List<Tuple<int, int>> Get()
        {
            //string s = "";
            //foreach (var tuple in GetData())
            //{
            //    s += string.Format("x={0},y={1}\n", tuple.Item1.ToString(), tuple.Item2.ToString());
            //}
            //return s;
            return GetData();
        }


        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}