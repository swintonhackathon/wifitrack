﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace GetMongoData.Controllers
{
    public class HourlyCountController : ApiController
    {
        public class HourlyCount
        {
            public int Hour { get; set; }
            public int Count { get; set; }
        }

        public class Location
        {
            public ObjectId _id { get; set; }
            public BsonDateTime timestamp { get; set; }
            public string mac { get; set; }
            public int x { get; set; }
            public int y { get; set; }
        }

        private List<HourlyCount> GetData()
        {
            const string connectionString = "mongodb://52.17.20.213";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("wifitracker");
            var collection = database.GetCollection<Location>("locations");

            var query = Query<Location>.GT(l => l.timestamp, DateTime.Now.AddDays(-1));
            var locations = collection.Find(query);
            var results = from loc in locations
                          group loc by new {loc.timestamp.ToUniversalTime().Hour}
                          into t
                          orderby t.Key.Hour
                          select new HourlyCount() {Count = t.Count(), Hour = t.Key.Hour};
            

            return results.ToList();

        }



        // GET api/hourlycount
        public List<HourlyCount> Get()
        {
            return GetData();
        }

    }
}
